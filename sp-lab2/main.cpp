#include <Windows.h>
#include "resource.h"

LPCWSTR windowClassName = L"sp_lab2";

const int BUTTON_WIDTH = 70; 
const int BUTTON_HEIGHT = 30;

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam);

void ResizeElements(HWND hwnd);

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance,
    LPSTR lpCmdLine, int nCmdShow)
{
    WNDCLASSEX wc;
    HWND hwnd;
    MSG msg;

    wc.cbSize = sizeof(WNDCLASSEX);
    wc.style = 1;
    wc.lpfnWndProc = WndProc;
    wc.cbClsExtra = 0;
    wc.cbWndExtra = 0;
    wc.hInstance = hInstance;
    wc.hIcon = LoadIcon(NULL, IDI_APPLICATION);
    wc.hCursor = LoadCursor(NULL, IDC_ARROW);
    wc.hbrBackground = (HBRUSH)(COLOR_WINDOW);
    wc.lpszMenuName = NULL;
    wc.lpszClassName = windowClassName;
    wc.hIconSm = NULL;

    if (!RegisterClassEx(&wc))
    {
        MessageBox(NULL, L"Couldn't register the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    hwnd = CreateWindowEx(
        WS_EX_CLIENTEDGE,
        windowClassName,
        L"������������ ������ �2",
        WS_OVERLAPPEDWINDOW,
        CW_USEDEFAULT, CW_USEDEFAULT, 600, 400,
        NULL, NULL, hInstance, NULL
    );

    if (hwnd == NULL)
    {
        MessageBox(NULL, L"Couldn't create the Window Class.", L"Error!",
            MB_ICONEXCLAMATION | MB_OK);
        return -1;
    }

    ShowWindow(hwnd, nCmdShow);
    UpdateWindow(hwnd);

    while (GetMessage(&msg, NULL, 0, 0) > 0)
    {
        TranslateMessage(&msg);
        DispatchMessage(&msg);
    }

    return msg.wParam;
}

LRESULT CALLBACK WndProc(HWND hwnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
    switch (msg)
    {
        case WM_CREATE:
        {
            CreateWindow(L"EDIT", NULL,
                WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_HSCROLL | ES_MULTILINE |
                ES_AUTOVSCROLL | ES_AUTOHSCROLL,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_EDIT, GetModuleHandle(NULL), NULL);

            CreateWindow(L"LISTBOX", NULL,
                WS_CHILD | WS_VISIBLE | LBS_STANDARD | LBS_HASSTRINGS,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_LB_LEFT, GetModuleHandle(NULL), NULL);

            CreateWindow(L"LISTBOX", NULL,
                WS_CHILD | WS_VISIBLE | LBS_STANDARD | LBS_HASSTRINGS,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_LB_RIGHT, GetModuleHandle(NULL), NULL);

            CreateWindow(L"BUTTON", L"Add",
                WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_BTN_ADD, GetModuleHandle(NULL), NULL);

            CreateWindow(L"BUTTON", L"Clear",
                WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_BTN_CLEAR, GetModuleHandle(NULL), NULL);

            CreateWindow(L"BUTTON", L"To Right",
                WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_BTN_TORIGHT, GetModuleHandle(NULL), NULL);

            CreateWindow(L"BUTTON", L"Delete",
                WS_TABSTOP | WS_VISIBLE | WS_CHILD | BS_DEFPUSHBUTTON,
                0, 0, 0, 0,
                hwnd, (HMENU)IDC_BTN_DELETE, GetModuleHandle(NULL), NULL);

            ResizeElements(hwnd);
            break;
        }
        case WM_COMMAND:
            switch (LOWORD(wParam))
            {
                case IDC_BTN_ADD:
                {
                    int len = GetWindowTextLength(GetDlgItem(hwnd, IDC_EDIT));
                    if (len > 0)
                    {
                        TCHAR* buf = (TCHAR*)GlobalAlloc(GPTR,
                            sizeof(TCHAR) * (len + 1));
                        GetDlgItemText(hwnd, IDC_EDIT, buf, len + 1);
                        if (
                            SendDlgItemMessage(
                                hwnd,
                                IDC_LB_LEFT,
                                LB_FINDSTRINGEXACT,
                                0, (LPARAM)buf
                            ) == LB_ERR)
                        {
                            SendDlgItemMessage(hwnd, IDC_LB_LEFT, LB_ADDSTRING,
                                (WPARAM)0, (LPARAM)buf);
                        }
                        GlobalFree(buf);
                    }
                    break;
                }
                case IDC_BTN_CLEAR:
                    SendDlgItemMessage(hwnd, IDC_LB_LEFT, LB_RESETCONTENT,
                        (WPARAM)0, (LPARAM)0);
                    SendDlgItemMessage(hwnd, IDC_LB_RIGHT, LB_RESETCONTENT,
                        (WPARAM)0, (LPARAM)0);
                    break;
                case IDC_BTN_TORIGHT:
                {
                    int index = SendDlgItemMessage(hwnd, IDC_LB_LEFT,
                        LB_GETCURSEL, (WPARAM)0, (LPARAM)0);
                    if (index != LB_ERR)
                    {
                        int len = SendDlgItemMessage(hwnd, IDC_LB_LEFT,
                            LB_GETTEXTLEN, (WPARAM)index, (LPARAM)0);
                        TCHAR* buf = (TCHAR*)GlobalAlloc(GPTR,
                            sizeof(TCHAR) * (len + 1));
                        SendDlgItemMessage(hwnd, IDC_LB_LEFT, LB_GETTEXT,
                            (WPARAM)index, (LPARAM)buf);
                        if (
                            SendDlgItemMessage(
                                hwnd,
                                IDC_LB_RIGHT,
                                LB_FINDSTRINGEXACT,
                                0, (LPARAM)buf
                            ) == LB_ERR)
                        {
                            SendDlgItemMessage(hwnd, IDC_LB_RIGHT, LB_ADDSTRING,
                                (WPARAM)0, (LPARAM)buf);
                        }
                        GlobalFree(buf);
                    }
                    else
                    {
                        MessageBox(hwnd, L"No item selected!", L"Error", MB_OK | MB_ICONEXCLAMATION);
                    }
                    break;
                }
                case IDC_BTN_DELETE:
                {
                    int index = SendDlgItemMessage(hwnd, IDC_LB_LEFT,
                        LB_GETCURSEL, (WPARAM)0, (LPARAM)0);
                    if (index != LB_ERR)
                    {
                        SendDlgItemMessage(hwnd, IDC_LB_LEFT, LB_DELETESTRING,
                            (WPARAM)index, (LPARAM)0);
                    }

                    index = SendDlgItemMessage(hwnd, IDC_LB_RIGHT,
                        LB_GETCURSEL, (WPARAM)0, (LPARAM)0);
                    if (index != LB_ERR)
                    {
                        SendDlgItemMessage(hwnd, IDC_LB_RIGHT, LB_DELETESTRING,
                            (WPARAM)index, (LPARAM)0);
                    }
                    break;
                }
            }
            break;
        case WM_SIZE:
            ResizeElements(hwnd);
            break;
        case WM_GETMINMAXINFO:
        {
            MINMAXINFO* mmi = (MINMAXINFO*)lParam;
            mmi->ptMinTrackSize.x = 600;
            mmi->ptMinTrackSize.y = 400;
            break;
        }
        case WM_CLOSE:
            DestroyWindow(hwnd);
            break;
        case WM_DESTROY:
            PostQuitMessage(0);
            break;
        default:
            return DefWindowProc(hwnd, msg, wParam, lParam);
    }
    return 0;
}

void ResizeElements(HWND hwnd)
{
    HWND hEdit = GetDlgItem(hwnd, IDC_EDIT);
    HWND hLbLeft = GetDlgItem(hwnd, IDC_LB_LEFT);
    HWND hLbRight = GetDlgItem(hwnd, IDC_LB_RIGHT);
    HWND hBtnAdd = GetDlgItem(hwnd, IDC_BTN_ADD);
    HWND hBtnClear = GetDlgItem(hwnd, IDC_BTN_CLEAR);
    HWND hBtnToRight = GetDlgItem(hwnd, IDC_BTN_TORIGHT);
    HWND hBtnDelete = GetDlgItem(hwnd, IDC_BTN_DELETE);

    RECT rect;
    GetClientRect(hwnd, &rect);

    int block_width = rect.right / 3;

    SetWindowPos(hEdit, NULL, 1, 1, block_width-1, rect.bottom-1,
        SWP_NOZORDER);

    int button_x = rect.right / 2 - BUTTON_WIDTH / 2;

    SetWindowPos(hBtnAdd, NULL, button_x, 0, BUTTON_WIDTH, BUTTON_HEIGHT,
        SWP_NOZORDER);
    SetWindowPos(hBtnClear, NULL, button_x, BUTTON_HEIGHT, BUTTON_WIDTH,
        BUTTON_HEIGHT, SWP_NOZORDER);
    SetWindowPos(hBtnToRight, NULL, button_x, BUTTON_HEIGHT * 2,
        BUTTON_WIDTH, BUTTON_HEIGHT, SWP_NOZORDER);
    SetWindowPos(hBtnDelete, NULL, button_x, BUTTON_HEIGHT * 3,
        BUTTON_WIDTH, BUTTON_HEIGHT, SWP_NOZORDER);

    int lb_width = block_width / 2;

    SetWindowPos(hLbLeft, NULL, block_width * 2, 1, lb_width,
        rect.bottom-1, SWP_NOZORDER);
    SetWindowPos(hLbRight, NULL, block_width * 2 + lb_width, 1, lb_width,
        rect.bottom-1, SWP_NOZORDER);
}
