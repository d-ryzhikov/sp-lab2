#pragma once

#define IDC_EDIT            101

#define IDC_LB_LEFT         102
#define IDC_LB_RIGHT        103

#define IDC_BTN_ADD         104
#define IDC_BTN_CLEAR       105
#define IDC_BTN_TORIGHT     106
#define IDC_BTN_DELETE      107
